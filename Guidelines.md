# Things to do

Join the GSoC mailing list. Introduce yourself over there so that the community can get to know you. Feel free to discuss your project ideas as well as to ask for any help that you may require.

Draft a detailed proposal regarding your project and submit it on the Google Summer of Code website.

If you need help with anything, ask on the GSoC list or our IRC channel [#indicproject on Freenode](irc://irc.freenode.net/indicproject).

Don't be afraid if you don't know a particualr tool. For example, git or SSH. We can teach you everything that is needed if you are enthusiastic and are willing to learn new things and do homework. So, and also, please do not expect us to spoon-feed you)

# Writing your proposal

If you submit a draft proposal we will be able to give feedback so you can improve your proposal.

Finally you have to submit your finalized proposal at GSoC website 

The application template is as given below

## Template

1. Personal information
2. Email Address:
3. Telephone:
4. Blog URL (if any):
5. Freenode IRC Nick:
6. Your university and current education:
7. Why do you want to work with the Indic Project?
8. Do you have any past involvement with the Indic project or another open source project as a contributor?
9. Did you participate with the past GSoC programs, if so which years, which organizations?
10. Do you have other obligations between May and August ? Please note that we expect the Summer of Code to be a full time, 40 hour a week commitment
11. Will you continue contributing/ supporting the Indic project after the GSoC 2016 program, if yes, which area(s), you are interested in?
12. Why should we choose you over other applicants?
13. Proposal Description: Please describe your proposal in detail.

**NOTE**: Please do not verbatim copy text from the ideas page, or from other people's discussions about your project, but rewrite it in your own words.If you include any significant text or code from another source in your application, it must be accompanied with a proper citation. All papers or references that you use or plan to use must also be cited. Put all this in a "References" section at the bottom of your application.

## Include:

- An overview of your proposal
- The need it fulfils or the problem it solves
- Any relevant experience you have
- How you intend to implement your proposal
- A rough timeline for your progress with phases
- Any other details you feel we should consider
- Tell us about something you have created.
- Have you communicated with a potential mentor? If so, who?
- Other requirements
- Make sure you have completed following task to get qualified, failing to complete any task will results in rejecting your application.

You have subscribed with the GSoC mailinglist and respective project mailing list (eg. Libindic mailinglist for Libindic projects)
Your application is submitted to Google Summer of Code application system
In addition to the written proposal, we require every GSoC accepted applicant to do this for their project:

Do create an account on the Indic Project discourse and start a thread for your project. Keep it updated with the latest details.
We expect every GSoC participant to post updates (If not, already) and post about their project's status, development, etc.
Update the project status in the mailing list regularly with a meaningful subject line (don't use something like 'GSoC Project Update')