# GSOC specific participation

Join in GSOC 2017 Mailinglist and send queries to : <gsoc@indicproject.org> [[join List](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)]

These messages can be viewed at the [corresponding archive in Google Groups](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)




# General project questions 

contact@indicproject.org