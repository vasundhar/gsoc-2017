Ideas for Google Summer of Code 2017
===================================

If you want to propose an idea, please write it to contact@indicproject.org or in GSoC Mailing list . [[Join list](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)] 

Ideas will be updated soon 

# Indic Keyboard

## Indic Keyboard SDK

There are several thirdparty developers interested in packing Indic Keyboard along with their app,
so that they don't need to worry about end user having installed an input method.


**Complexity** - Difficult

**Confirmed Mentors(s)** - Jishnu Mohan

**How to contact the mentor** - jishnu7 on IRC

**Expertise required** - Java, Android Development

**Related Links**
 - 
 -

## Bluetooth/USB Keyboard support

As title suggests, indic Keyboard currently supports only virtual keyboards.
This task is to support physical keyboards.


**Complexity** - Medium

**Confirmed Mentors(s)** - Jishnu Mohan

**How to contact the mentor** - jishnu7 on IRC

**Expertise required** - Java, Android Development

**Related Links** 
 - 
 -

# Varnam Project

## Make the learnings file common for all schemes under a language

Assume there are 2 schemes for a language. `langcode-phonetic` and `langcode-inscript`. Today, Varnam needs to learn all the words separately for each scheme. Ideally, we should be able to reuse the learnings for a language across different schemes.

This involves changing the learning system, and transliteration system in Varnam. For more details, visit this [issue](https://github.com/varnamproject/libvarnam/issues/141) on `libvarnam`.

Description

**Complexity** - Difficult

**Confirmed Mentors(s)** - Navaneeth K N

**How to contact the mentor** - navaneethkn at gmail

**Expertise required** - C, algorithms, databases

**Related Links**
 - [Website](https://www.varnamproject.com)
 - [Source code](https://github.com/varnamproject)
 - [Issue on libvarnam](https://github.com/varnamproject/libvarnam/issues/141)
 


# Libindic

## Port LibIndic to Python3 and adapt REST API to match it

Many modules in LibIndic currently doesn't have Python 3 support. Since it has been officially announced that Python 2 will be deprecated soon, it is better to update the LibIndic codebase to Python 3. This also involves proper namespacing of packages, which is currently in a partial stage. So, this idea involves three steps
 * Port code to support Python 3.
 * Properly namespace the packages
 * Update the REST API to consider these changes

**Complexity** - Easy

**Confirmed Mentors(s)** - Balasankar C

**How to contact the mentor** - balasankarc AT autistici DOT org

**Expertise required** - Python, Concepts of REST.

**Related Links** 
 - [LibIndic Projects](http://github.com/libindic/)
 - [PEP 420 - Namespace Packages](https://www.python.org/dev/peps/pep-0420/)

## Improve Sandhi Splitter

Sandhi-Splitter currenty uses a Naive Bayesian strategy. Explore the possibilities of using RNN (Recurrent Neural Network) strategies like LSTM (Long Short Term Memory) as an alternative.

**Complexity** - Intermediate

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** - Python, AI - specifically Neural Network concepts.

**Related Links** 
 - [Sandhi Splitter module](https://github.com/libindic/sandhi-splitter)
 -

# Android SDKs

## Indic Keyboard SDK

Description

**Complexity** -

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** -

**Related Links** 
 - 
 -

## libindic-SDK improvements

Description

**Complexity** -

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** -

**Related Links** 
 - 
 -

# Ibus-Braille

## Idea1

Description

**Complexity** -

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** -

**Related Links** 
 - 
 -

# Indicjs

## Porting libindic to javascript

Javascript modules that can run on both server and client will let wider adoption of indic language tools on the web. The idea is to port existing libindic modules from python to javascript (preferably ES6) and have a suite of such modules. 

**Complexity** - Easy to Medium

**Confirmed Mentors(s)** - [Akshay S Dinesh](http://asd.learnlearn.in)

**How to contact the mentor** - ping asdofindia in #indicproject

**Expertise required** - javascript, ux

**Related Links**
 - [indicjs prototype](https://github.com/indicjs)
 - [more ideas on spec](https://pads.inflo.ws/p/r.7655a1d5fbcf2df9d92d1110b67acba4)

## Automatic creation of ascii unicode conversion maps from ttf files of ascii fonts

Character recognition is good enough to allow computers detect characters from standard fonts. We can use this to generate the ascii-unicode conversion map for any given TTF thereby eliminating the need for manual conversion of new fonts.

**Complexity** - Easy to Medium

**Confirmed Mentors(s)** - [Akshay S Dinesh](http://asd.learnlearn.in)

**How to contact the mentor** - ping asdofindia in #indicproject

**Expertise required** - OCR algorithms, font standards

**Related Links** 
 - [opentype.js](https://github.com/nodebox/opentype.js)
 - [Sketchy.js](https://github.com/kjkjava/Sketchy.js)

# Dhvani


## Idea1

Description

**Complexity** -

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** -

**Related Links** 
 - 
 -

# Grandham

## Idea1

Description

**Complexity** -

**Confirmed Mentors(s)** -

**How to contact the mentor** -

**Expertise required** -

**Related Links** 
 - 
 -









